package main;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;
import javax.swing.JTextField;
import java.awt.CardLayout;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JLabel;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.JList;

public class MainWindowApp {

	private JFrame frame;
	private JTextField textField;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindowApp window = new MainWindowApp();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindowApp() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 601, 433);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().setBackground(java.awt.Color.white);
		
		textField = new JTextField();
		textField.setBounds(12, 335, 114, 19);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Buscar");
		btnNewButton.setBounds(9, 366, 117, 25);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(12, 308, 70, 15);
		frame.getContentPane().add(lblNombre);
		
		
		
		JLabel lblNewLabel = new JLabel("");
		
		
		lblNewLabel.setBounds(161, 31, 428, 75);
		frame.getContentPane().add(lblNewLabel);
		
		
		ImageIcon imageIcon = new ImageIcon("./img/udc.png"); // load the image to a imageIcon
		Image img = imageIcon.getImage(); // transform it 
		Image newimg = img.getScaledInstance(428, 75,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		lblNewLabel.setIcon(new ImageIcon(newimg));
		
		JSeparator separator = new JSeparator();
		separator.setForeground(Color.BLACK);
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBackground(Color.RED);
		separator.setBounds(138, 50, 1, 328);
		frame.getContentPane().add(separator);
		
		JPanel j = new JPanel();
		j.setBounds(161, 142, 391, 224);
		frame.getContentPane().add(j);
		
		String[] colNames = {"Item", "Count"};
		DefaultTableModel dtm = new DefaultTableModel(colNames, 0);

		
		JTable t = new JTable(dtm);
		t.setBackground(Color.GREEN);

		t.getTableHeader().setReorderingAllowed(false);
		t.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

		t.getColumnModel().getColumn(0).setPreferredWidth(113); 

		j.add(new JScrollPane(t), BorderLayout.CENTER);
		
	
		 
	}
}

/* 
   Created on : Dec, 2016
   Author     : Marcos Horro Varela, Avelino Ríos Sáez
*/
var logicModule = (function () {
    var server = "http://localhost:8080/cgi-bin/client-requests.py";
    var ajax = ajaxModule;

    var currentStart = 0;
    var numPages = 1;

    var lastParams = "";

    function resetPages() {
	currentStart = 0;
	numPages = 1;
    }
    
    var dom = {
        searchLink:  document.querySelector("#searchBtn"),
        itemLink:  document.querySelector("#boton"),
        returnLink:  document.querySelector("#boton2"),
    };
    
    function init() {
	bindUIAction();
        $("#completeFields").hide();
        $("#results").hide();
	$("#noresults").hide();
    }

    function bindUIAction() {
	dom.searchLink.onclick = doSearch;
	dom.itemLink.onclick = divFunction;
	dom.returnLink.onclick = divFunction2;
    }


    function detailsPlayer(){
	$('.table > tbody > tr').click(function() {
	    var row = $(this).find('td:first');
	    console.log(tr);
	    
	    ajax.post(server, "&action=details&id=" + $(this)[0].id, playerCallback);
	    $(inicio).hide();
	    $(jugador).show();
	});
    }

    
    function divFunction(){
    	$(jugador).hide();
    	$(inicio).show();
  	
    }

    function divFunction2(){
    	$(alumnos).hide();
	$(jugador).hide();
    	$(inicio).show();
    }

    $(btnAlumnos).click(function() {
  	$(inicio).hide();
	$(jugador).hide();
  	$(alumnos).show();
    });

    function drawCircle(media) {
	$("#test-circle").circliful({
	    animation: 1,
	    animationStep: 5,
	    foregroundBorderWidth: 15,
	    backgroundBorderWidth: 15,
	    percent: media,
	    textSize: 28,
	    textStyle: 'font-size: 12px;',
	    textColor: '#666',
	    multiPercentage: 1,
	    percentages: [10, 20, 30]
	});
    }

    function doSearch() {
	console.log("doSearch");
	$("#completeFields").hide();
        $("#results").show();

	resetPages();

	var name = $('#inputNombre').val();
        var ageMin = $('#rangeAge').data('slider').getValue()[0];
        var ageMax = $('#rangeAge').data('slider').getValue()[1];
	var team = $("#inputEquipo").val();
	var currentTeam = false;
	if ($("#currentTeam").is(":checked")) {
	    currentTeam = true;
	};
	var position = $("#posicion").val();
	var nacionality = $("#selectNacionalidad").val();
	var pierna = $("#pierna").val();

	//compruebo que no mando valores vacíos
	if (name===""){
	    name = "todos"
	}
	if (team===""){
	    team = "todos"
	}
        if (nacionality==="Selecciona la nacionalidad"){
	    nacionality = "todos"
	}

	console.log("name: " + name);
	console.log("team: " + team);
	console.log("position: " + position);
	console.log("nacionality: " + nacionality);
	console.log("pierna: " + pierna);

	if (!(name==="todos" && team==="todos" && position==="a" &&
	      pierna==="N" && nacionality==="todos")){
	    var params = "action=search&name=" + name +
		"&ageMin=" + ageMin + "&ageMax=" + ageMax +
		"&team=" + team + "&currentTeam=" + currentTeam +
		"&position=" + position + "&nacionality=" + nacionality + 
		"&foot=" + pierna;
	    lastParams = params;
	    params = params + "&start=" + currentStart;
            var node = document.getElementById("params-search");
            node.innerHTML = '<b>Nombre</b>: ' + name +
                '; <b>Equipo</b>: ' + team + '; <b>Edad</b>: ' + ageMin +
                '-' + ageMax + '; <b>Posición</b>: ' + position +
                '; <b>Nacionalidad</b>: ' + nacionality +
                '; <b>Pierna</b>: ' + pierna;
	    
	    $('#pagination').twbsPagination('destroy');
	    ajax.post(server, params, searchCallback);
	} else {
	    $("#completeFields").show();
	}
    }

    function searchPage(page) {
	var params = lastParams + "&start=" + currentStart;
	ajax.post(server, params, searchCallback);
    }
    
    function encode_utf8(s) {
	return unescape(encodeURIComponent(s));
    }
    
    function decode_utf8(s) {
	return decodeURIComponent(escape(s));
    }
    
    function playerCallback(resp){
	console.log(resp);
	var myplayer = JSON.parse(resp);

	$('#averageGraph').html(""); 
	$('#averageGraph').html('<div class="row"><div id="test-circle"></div></div><div class="row"><h3 style="text-align:center"><b>Media</b></h3></div>');
	drawCircle(myplayer.response.docs[0].meta_playeraverage);
	var caja = document.getElementById('playerData');
	
	/* Name */
	document.getElementById("player-name-bread").innerHTML = myplayer.response.docs[0].meta_playername;
	var name = document.getElementById('playerName');
    	name.innerHTML = myplayer.response.docs[0].meta_playername;

    	/* Nationality */
    	var nat = document.getElementById('playerNat');
    	nat.innerHTML = myplayer.response.docs[0].meta_playernacionality;

    	/* Current team */
    	var club = document.getElementById('playerClub');
    	club.innerHTML = myplayer.response.docs[0].meta_playerclub;

    	/* Panels */
	$("#playerWeight").html(myplayer.response.docs[0].meta_playerweight);
	$("#playerHeight").html(myplayer.response.docs[0].meta_playerheight);
	$("#playerPos").html(myplayer.response.docs[0].meta_playerposition);
	$("#playerFoot").html(myplayer.response.docs[0].meta_playerfoot);

    	$('#boxPhoto').html(""); 
	var boxPhoto = document.getElementById('boxPhoto');

	console.log(myplayer.response.docs[0]);
	img = document.createElement('img');
	img.setAttribute("src", myplayer.response.docs[0].meta_playerimg);
	img.setAttribute("class", "img-circle");
	img.setAttribute("height", "120");
	img.setAttribute("width", "120");
	img.setAttribute("alt", "Avatar");
	boxPhoto.appendChild(img); 

    	p = document.createElement('hr');
    	boxPhoto.appendChild(p);
	
	$('#boxTeams > tbody > tr').remove(); 
	var boxTeams = document.getElementById('boxTeams');
	pEquipos = document.createElement('p');

	var string = myplayer.response.docs[0].meta_playerhist;

	var table = document.getElementById('boxTeams').getElementsByTagName('tbody')[0];
	var array = encode_utf8(string).split(",");
	for (i=0; i<array.length-1;i++) {
	    console.log(line);
	    tr = document.createElement('tr');
	    var line = array[i].split(":");
	    
	    tdYear = document.createElement('td');
	    tdYear.innerHTML = line[0];
	    tr.appendChild(tdYear);

	    tdTeam = document.createElement('td');
	    tdTeam.innerHTML = line[1];
	    tr.appendChild(tdTeam);
	    table.appendChild(tr);
	}
    }
    
    function searchCallback(resp) {
	console.log(resp);
	var myjson = JSON.parse(resp);

	$('.table > tbody > tr').remove(); 
	var table = document.getElementById('tableList').getElementsByTagName('tbody')[0];
	var count = 0;
	_.each(myjson.response.docs, function(item) { 
    	    tr = document.createElement('tr');

    	    tr.id = item.id;

	    if (item.meta_playername==="") { return; }
	    
    	    tdName = document.createElement('td');
    	    tdName.innerHTML = item.meta_playername;
    	    tr.appendChild(tdName);

    	    tdNacionalidad = document.createElement('td');
    	    tdNacionalidad.innerHTML = item.meta_playernacionality;
    	    tr.appendChild(tdNacionalidad);

    	    tdPosicion = document.createElement('td');
    	    tdPosicion.innerHTML = item.meta_playerposition;
    	    tr.appendChild(tdPosicion);

    	    tdMedia = document.createElement('td');
    	    tdMedia.innerHTML = item.meta_playeraverage;
    	    tr.appendChild(tdMedia);

	    table.appendChild(tr);
	    count = count + 1;
        });
	detailsPlayer();
	console.log(count);
	if (count==0) {
	    $("#tableList").hide();
	    $("#noresults").show()
	} else {
	    $("#noresults").hide();
	    $("#tableList").show();
	    numPages = myjson.response.numFound / 10;
	    var nVisiblePages = 10;
	    if (numPages<10) { nVisiblePages = numPages; }
	    $(function () {
		window.pagObj = $('#pagination').twbsPagination({
		    totalPages: numPages,
		    visiblePages: nVisiblePages,
		    initiateStartPageClick: false,
		    onPageClick: function (event, page) {
			event.preventDefault();
			currentStart = page*10;
			searchPage(currentStart);
		    }
		});
	    });
	}
    }
    
    return {
	init: init
    }
})();

logicModule.init();

///////////////////////////////////////////
// DEBUG
	var myplayer = {
	    "responseHeader": {
		"status": 0,
		"QTime": 1,
		"params": {
		    "q": "*:*",
		    "indent": "true",
		    "fl": "meta_playerhist,meta_playername,meta_playerheight,meta_playerposition,meta_playerfoot,meta_playeraverage,",
		    "wt": "json",
		    "_": "1481470432917"
		}
	    },
	    "response": {
		"numFound": 1,
		"start": 0,
		"docs": [
		    {
		        "meta_playerhist": "2011:Arsenal;2008:Arsenal;",
		        "meta_playername": "Jens LEHMANN",
		        "meta_playerheight": "N/A",
		        "meta_playeraverage": "87",
		        "meta_playerfoot": "Derecho",
		        "meta_playerposition": "PO",
		        "id": "suuuuuuuuuuuuuuuuuuuuuuuuu",
		        "imagen": "http://cdn.soccerwiki.org/images/player/41780.jpg"
		    }
		]
	    }
	}

	var myjson = {
	    "responseHeader": {
		"status": 0,
		"QTime": 1,
		"params": {
		    "q": "*:*",
		    "indent": "true",
		    "fl": "meta_playerhist,meta_playername,meta_playerheight,meta_playerposition,meta_playerfoot,meta_playeraverage,",
		    "wt": "json",
		    "_": "1481470432917"
		}
	    },
	    "response": {
		"numFound": 9,
		"start": 0,
		"docs": [
		    {
		        "meta_playerhist": "2011:Arsenal;2008:Arsenal;",
		        "meta_playername": "Jens LEHMANN",
		        "meta_playerheight": "N/A",
		        "meta_playeraverage": "87",
		        "meta_playerfoot": "Derecho",
		        "meta_playerposition": "PO",
		        "id": "suuuuuuuuuuuuuuuuuuuuuuuuu"
 		    },
		    {
		        "meta_playerhist": "2015:Barcelona;2014:Barcelona;2013:Sevilla;",
		        "meta_playername": "Ivan Rakitić",
		        "meta_playerheight": "184",
		        "meta_playeraverage": "93",
		        "meta_playerfoot": "Ambidiestro",
		        "meta_playerposition": "M(C),MO(DerIzqC)"
		    },
		    {
		        "meta_playerhist": "2015:Real Madrid;2012:Real Madrid;",
		        "meta_playername": "Cristiano Ronaldo Dos Santos Aveiro",
		        "meta_playerheight": "185",
		        "meta_playeraverage": "99",
		        "meta_playerfoot": "Ambidiestro",
		        "meta_playerposition": "MO(DerIzq),A(DerIzqC)"
		    },
		    {
		        "meta_playerhist": "2015:Arsenal;2014:Arsenal;2012:Barcelona;2011:Udinese Calcio;",
		        "meta_playername": "Alexis Alejandro Sánchez",
		        "meta_playerheight": "170",
		        "meta_playeraverage": "94",
		        "meta_playerfoot": "Derecho",
		        "meta_playerposition": "MO,A(DerIzqC)"
		    },
		    {
		        "meta_playerhist": "2013:Real Madrid;2012:Tottenham Hotspur;2011:Tottenham Hotspur;",
		        "meta_playername": "Gareth Frank Bale",
		        "meta_playerheight": "183",
		        "meta_playeraverage": "94",
		        "meta_playerfoot": "Izquierdo",
		        "meta_playerposition": "MO,A(DerIzqC)"
		    },
		    {
		        "meta_playerhist": "2016:Juventus;2013:SSC Napoli;2011:Real Madrid;",
		        "meta_playername": "Gonzalo Gerardo Higuaín",
		        "meta_playerheight": "184",
		        "meta_playeraverage": "94",
		        "meta_playerfoot": "Derecho",
		        "meta_playerposition": "A(DerIzqC)"
		    },
		    {
		        "meta_playerhist": "2016:AS Roma;2015:AS Roma;2012:Manchester City;2010:VfL Wolfsburg;",
		        "meta_playername": "Edin Džeko",
		        "meta_playerheight": "193",
		        "meta_playeraverage": "91",
		        "meta_playerfoot": "Ambidiestro",
		        "meta_playerposition": "A(C)",
		        "id": "diversion"
		    },
		    {
		        "meta_playerfoot": "",
		        "meta_playerhist": "",
		        "meta_playerposition": "",
		        "meta_playeraverage": ""
		    },
		    {
		        "meta_playerhist": "2014:Arsenal;2012:Arsenal;2011:Montpellier HSC;",
		        "meta_playername": "Olivier Giroud",
		        "meta_playerheight": "193",
		        "meta_playeraverage": "91",
		        "meta_playerfoot": "Izquierdo",
		        "meta_playerposition": "A(C)"
		    }
		]
	    }
	}

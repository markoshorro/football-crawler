#!/usr/bin/env python
# -*- coding: utf-8 -*-
import cgi
import Cookie
import os
from serverfacade import Model

# Parse request params
def get_params():
    form = cgi.FieldStorage()
    action = None
    params = None
    if form.has_key("action"):
        action = form["action"].value
        if action == "search":
            params = dict(page=form["start"].value,
                          name=form["name"].value,
			  agemin=form["ageMin"].value,
                          agemax=form["ageMax"].value,
                          team=form["team"].value,
                          currentteam=form["currentTeam"].value,
                          foot=form["foot"].value,
                          position=form["position"].value,
                          nacionality=form["nacionality"].value)
            return action, params
        if action == "details":
            params = form["id"].value
    return action, params

# requests to serverfacade
def do_search(params):
    model = Model()
    response = model.get_list(params)
    if response:
        return response
    else:
        return "{'error': 'connection error'}", None

# requests to serverfacade
def get_details(url):
    model = Model()
    response = model.get_details(url)
    if response:
        return response
    else:
        return "{'error': 'connection error'}", None
    

# ###### #
#  MAIN  #
# ###### #
def main():
    action, params = get_params()
    if action:
        if action == "search": # login returns a new cookie
            response = do_search(params)
        if action == "details":
            response = get_details(params)
        return response
    return "{'error': 'incorrect url'}"

try:
    #cookie_string = os.environ.get('HTTP_COOKIE') # get the cookie
    response = main()
    print 'Content-Type: application/json; charset=utf-8\n\n'
    print response.encode('UTF-8')
except:
    cgi.print_exception()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import string
import urllib2
import json
import unicodedata
import HTMLParser
import operator
import ast,sys
import re
import codecs
import sys
from urllib import quote

from htmlentitydefs import codepoint2name, name2codepoint

# DEVELOPMENT BD

def encode(source):
    new_source = ''
    s = unicode(source)
    for char in s.encode('iso-8859-15'):
        if ord(char) in codepoint2name:
            char = "&%s;" % codepoint2name[ord(char)]
        new_source += char

    return new_source

# PRODUCTION BD
fl_details = "fl=meta_playerhist%2Cmeta_playername%2Cmeta_playerheight%2Cmeta_playeraverage%2Cmeta_playerage%2Cmeta_playerfoot%2Cmeta_playerweight%2Cmeta_playerposition%2Cmeta_playernacionality%2Cmeta_playerclub%2Cmeta_playerimg&"

fl_list = "fl=meta_playername%2Cmeta_playeraverage%2Cmeta_playerposition%2Cmeta_playernacionality%2Cmeta_playerclub%2Cid&"

formatResp = "wt=json&indent=true"

SERVER = "http://localhost:8983/solr/collection1/select?q=*%3A*&"

class Model(object):

    def __init__(self):
        pass

    def generate_query_url_list(self, params):
        url = SERVER + "start="
        url += params['page'] + "&"
        if params['name']!="todos":
            parts = string.split(params['name'], " ")
            for p in parts:
                url += "fq=meta_playername%3A" + quote(encode(p)) + "&"
        if params['agemin']!="" and params['agemax']!="":
            url += "fq=meta_playerage%3A%5B" + params['agemin'] +"+TO+" + params['agemax'] +"%5D&"
        if params['team']!="todos":
            parts = string.split(params['team'], " ")
            for p in parts:
                url += "fq=meta_playerclub%3A" + quote(encode(p)) + "&"
                if params['currentteam']=="true":
                    url += "fq=meta_playerhist%3A" + quote(encode(p)) + "&"
        if params['foot']!="N":
            url += "fq=meta_playerfoot%3A" + params['foot'] + "&"
        if params['position']!="N":
            url += "fq=meta_playerposition%3A" + params['position'] + "&"
        if params['nacionality']!="todos":
            url += "fq=meta_playernacionality%3A" + quote(encode(params['nacionality']))  + "&"
            
        return url + fl_list + formatResp

    def generate_query_url_details(self, identifier):
        url = SERVER
        if identifier!="_":
            url += "fq=id%3A%22" + quote(identifier, safe='') + "%22&"
        return url + fl_details + formatResp

    def get_list(self, params):
        try:
            reload(sys)
            sys.setdefaultencoding('utf-8')
            full_url=Model().generate_query_url_list(params)
            #response = urllib2.urlopen(full_url)
            response = requests.get(full_url)
            #tmpres=response.read()
            #json_response = json.loads(tmpres)
            return json.dumps(response.json(),ensure_ascii=False)
        except requests.exceptions.ConnectionError:
            return None
        
    def get_details(self, url):
        try:
            full_url=Model().generate_query_url_details(url)
            response = requests.get(full_url)
            #tmpres=response.read()
            #json_response = json.loads(tmpres)
            return json.dumps(response.json(),ensure_ascii=False)
        except requests.exceptions.ConnectionError:
            return None
